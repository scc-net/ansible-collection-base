# Ansible Role: scc\_net.base.sssd

Installs and configures sssd to use ldap for user auth.

## Dependencies

 - none

## Role Variables

The sssd-config is split up in different categorie blocks.
Each block of variables corrosponds to a different block
in the sssd-config.

Each config variable is part of the `sssd` config dict.

Each variable is defined in this README with its default value.
Optional values are marked as such.

### SSSD-Block

```
custom_domain = 'LDAP' # optional

# Only relevant in Deb <= 10
services =
  - nss
  - pam
```

### Domain-Block

```
debug_level = '0'
cache_credentials = # optional

ldap_auth_password =
ldap_auth_bind = 
ldap_chpass_uri =
ldap_tls_cacert = # optional
ldap_uri_list =
ldap_search_base =
ldap_user_search_base = # optional
ldap_group_search_base = # optional
ldap_user_object_class = # optional
ldap_user_ssh_public_key = # optional
ldap_id_use_start_tls = # optional

ldap_schema =

ldap_allow_groups = # optional
ldap_allow_users = # optional
```

## Example Config

This example show the current default sssd config in our main ansible.

```
sssd:
  ldap_schema: rfc2307
  ldap_auth_bind: "cn=sssd,cn=root,ou=tmn,ou=scc,o=kit,dc=edu"
  ldap_auth_password: "{{ passwords.sssd }}"
  ldap_chpass_uri: "ldaps://net-entry-cn.tmn.scc.kit.edu"
  ldap_group_search_base: "ou=Group,ou=tmn,ou=scc,o=kit,dc=edu"
  ldap_search_base: "ou=tmn,ou=scc,o=kit,dc=edu"
  ldap_tls_cacert: /etc/ssl/certs/T-TeleSec_GlobalRoot_Class_2.pem
  ldap_uri_list: "{{ sssd_ldap_uri }}"
  ldap_user_search_base: "ou=People,ou=tmn,ou=scc,o=kit,dc=edu"


sssd_ldap_uri:
  - ldaps://net-entry-cs.tmn.scc.kit.edu
  - ldaps://net-entry-cn.tmn.scc.kit.edu
```
