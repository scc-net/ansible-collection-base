# Ansible Role: scc\_net.base.apt\_keys

Add authorized admin keys using `ansible.posix.authorized_key`
module.

## Dependencies

 - none

## Role Variables

```yaml
admin_keys_key_map:
  admin1:
    pc1: ssh-ed25519...
    laptop1: ssh-ed25519...
  admin2:
    pc2: ssh-ed25519...
  admin3:
    laptop1: ssh-ed25519...
admin_keys_admin_list:
  - admin1
  - admin2
admin_keys_user: root
```
