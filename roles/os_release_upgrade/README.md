# Ansible Role: scc_net.base.os_release_upgrade

Upgrade your system to the next major release.

## Dependencies

 - none

## Role Variables

 - none
