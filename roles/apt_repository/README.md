# Ansible Role: scc\_net.base.apt\_repository

Add keys annd remote repositories to Advanced Package Tool (apt).
Also allows the pinning of certain packages.

## Dependencies

 - none

## Role Variables

The role can be configured by setting the variable `apt_repository_list`.
See the [example](#example) on how to use it.

The following variables can be set to configure this role:

`id`: fingerprint of gpg-ke
`url`: direct download url of gpg-key
```yaml
key:
  dearmor:
  id:           # required
  keyring:
  url:          # required
```

This is the default for key keyring, apt sources and X-Rrepolib-Name
```yml
name: "{{  item.repo.url | urlsplit | (__url_splitted.hostname ~ __url_splitted.path) | regex_replace('(\\.|/)', '_') | trim('_') }}"
```

Variables represent following [deb822 options](https://manpages.debian.org/bookworm/apt/sources.list.5.en.html#THE_DEB_AND_DEB-SRC_TYPES:_OPTIONS)  
`filename`: set name of sources file
```yaml
repo:
  additional_options: # dict of additional options
  components: main
  enabled: yes
  filename: # defaults to name
  suites: {{ ansible_distribution_release | lower }}
  types: deb
  url:        # required
```

```yaml
pinning:
  package:
  pin:
  pin_priority:
  filename:
```

## Example

```yaml
dependencies:
  - role: scc_net.base.apt_repository
    vars:
      apt_repository_list:
        - name: dnsdist
          key:
            id: 9FAAA5577E8FCF62093D036C1B0C6205FD380FBB
            url: https://repo.powerdns.com/FD380FBB-pub.asc
          repo:
            url: http://repo.powerdns.com/debian
            components: main
            suites: "{{ ansible_distribution_release }}-dnsdist-{{ dnsdist_version }}"
          pinning:
            package: "dnsdist*"
            pin: origin repo.powerdns.com
            pin_priority: 600
        - key:
            id: "FFD65A0DA2BEBDEB73D44C8BB4D2D216F1FD7806"
            url: "https://download.jitsi.org/jitsi-key.gpg.key"
          repo:
            url: "deb https://download.jitsi.org stable/"
```
