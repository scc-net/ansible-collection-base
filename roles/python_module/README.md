**Variables**:
* `python_module_package_name`: Package to install
* `python_module_registry`: Registry to search for packet in
* `python_module_venv_path`: Path where the virtual env should be created
* `python_module_executables`: List of executables of the python module that will be symlinked into PATH
* `python_module_use_site_packages`: Enable system site-packages in venv (default: `False`)
* `python_module_install_dependencies`: Install package dependencies also via pip (default: `False`)
