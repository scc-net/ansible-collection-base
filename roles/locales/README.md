# Ansible Role: scc\_net.base.locales

Set default locales.

## Dependencies

 - none

## Role Variables

```
locales_default: "en_US.UTF-8"
locales_language: "en"

locales_global_default:
  LANG: "{{ locales_default }}"
  LANGUAGE: "{{ locales_default.split('.')[0] }}:{{ locales_language }}"
  LC_CTYPE: ""
  LC_NUMERIC: ""
  LC_TIME: ""
  LC_COLLATE: ""
  LC_MONETARY: ""
  LC_MESSAGES: ""
  LC_PAPER: ""
  LC_NAME: ""
  LC_ADDRESS: ""
  LC_TELEPHONE: ""
  LC_MEASUREMENT: ""
  LC_IDENTIFICATION: ""
  LC_ALL: ""

locales_global: {}
```
