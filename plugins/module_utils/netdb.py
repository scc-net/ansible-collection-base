DOCUMENTATION = """
        lookup: netdb template module
        short_description: queries netdb webapi
        description:
            - Template Module
        options:
          _terms:
            description: TBD
            required: True
          configfile:
            description: config-file path for netdb config
            type: string
            default: '~/.config/netdb_client.ini'
            ini:
              - section: netdb
                key: configfile
            env:
              - name: NETDB_CONFIGFILE
"""
import configparser
import os

from ansible.errors import AnsibleError
from ansible.module_utils._text import to_native
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from netdb_client.api41 import APISession, APIEndpoint

display = Display()


class NetdbLookupModule(LookupBase):
    api = None

    def _init_netdb(self):
        if self.api is None:
            display.v("Init netdb connection...")
            apiconfig = configparser.ConfigParser()
            apiconfig.read(os.path.expanduser(self.get_option("configfile")))
            endpoint = apiconfig.get('DEFAULT', 'endpoint')
            try:
                base_url = apiconfig.get(endpoint, "base_url")
            except configparser.NoOptionError:
                base_url = endpoint
            endpoint = APIEndpoint(
                base_url=base_url,
                token=apiconfig.get(endpoint, "token")
            )
            display.vv(f"Init netdb session with options: {endpoint.__dict__}")
            try:
                self.api = APISession(endpoint)
            except Exception as e:
                raise AnsibleError(f"API Init failed. Original exception: {to_native(e)}")
        else:
            display.vv("Netdb session already initialized...")
            return

    def run(self, terms, variables=None, **kwargs):
        raise NotImplementedError('Do not use this class directly!')
