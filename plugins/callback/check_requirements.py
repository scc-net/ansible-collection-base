import json
import os.path
import sys

import ansible.constants as C
import yaml
from ansible.module_utils.compat.importlib import import_module
from ansible.plugins.callback import CallbackBase

DOCUMENTATION = '''
name: check_requirements
callback_type: check
requirements:
    - enable in configuration
author: Dominik Rimpf
version_added: "0.3.0"
short_description: Retrieves the version of an installed collection
description:
    - Checks the galaxy requirements and errors if they not matchoptions:
options:
  requirements_file:
    description: File to load the requirements from
    type: string
    default: "requirements.yml"
    ini:
      - section: check
        key: requirements_file
'''


# This was taken from https://github.com/ansible-collections/community.general/blob/main/plugins/lookup/collection_version.py
# BEGIN
def load_collection_meta_manifest(manifest_path):
    with open(manifest_path, 'rb') as f:
        meta = json.load(f)
    return {
        'version': meta['collection_info']['version'],
    }


def load_collection_meta_galaxy(galaxy_path, no_version='*'):
    with open(galaxy_path, 'rb') as f:
        meta = yaml.safe_load(f)
    return {
        'version': meta.get('version') or no_version,
    }


def load_collection_meta(collection_pkg, no_version='*'):
    path = os.path.dirname(collection_pkg.__file__)

    # Try to load MANIFEST.json
    manifest_path = os.path.join(path, 'MANIFEST.json')
    if os.path.exists(manifest_path):
        return load_collection_meta_manifest(manifest_path)

    # Try to load galaxy.y(a)ml
    galaxy_path = os.path.join(path, 'galaxy.yml')
    galaxy_alt_path = os.path.join(path, 'galaxy.yaml')
    # galaxy.yaml was only supported in ansible-base 2.10 and ansible-core 2.11. Support was removed
    # in https://github.com/ansible/ansible/commit/595413d11346b6f26bb3d9df2d8e05f2747508a3 for
    # ansible-core 2.12.
    for path in (galaxy_path, galaxy_alt_path):
        if os.path.exists(path):
            return load_collection_meta_galaxy(path, no_version=no_version)

    return {}
# END


class CallbackModule(CallbackBase):
    """
    This callback module checks if all galaxy requirements are up to date
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'check'
    CALLBACK_NAME = 'scc_net.base.check_requirements'
    CALLBACK_NEEDS_ENABLED = True

    def __init__(self, display=None, options=None):
        # make sure the expected objects are present, calling the base's __init__
        super(CallbackModule, self).__init__(display=display, options=options)

        if not self._plugin_options:
            self.set_options()

        requirements_file_config = self.get_option('requirements_file')
        requirements_file_path = os.path.abspath(os.path.expanduser(os.path.expandvars(requirements_file_config)))
        self._display.vv(f'Loading requirements file {requirements_file_path}')
        with open(requirements_file_path) as infile:
            self.requirements = yaml.safe_load(infile)

        self.check()

    def check(self):
        self._display.v('Checking requirements...')

        role_errors = []
        for role in self.requirements.get('roles', []):
            try:
                role_name = role['name']
                version_wanted = role['version']
            except KeyError as err:
                role_errors.append(f'Error in role definition. Missing key: {err}')
                continue

            search_paths = getattr(C, 'DEFAULT_ROLES_PATH')
            role_path_candidates = [
                os.path.join(os.path.abspath(os.path.expanduser(os.path.expandvars(path))), role_name) for path in
                search_paths]
            existing_role_paths = [path for path in role_path_candidates if os.path.isdir(path)]

            if not existing_role_paths:
                role_errors.append(f'Could not find role "{role_name}" in search paths: {search_paths}')
                continue

            with open(os.path.join(str(existing_role_paths[0]), 'meta/.galaxy_install_info')) as infile:
                data = yaml.safe_load(infile)
            version_installed = data['version']

            if version_installed != version_wanted:
                role_errors.append(f'Role "{role_name}" is not installed with the correct version. '
                                   f'Installed: {version_installed} Wanted: {version_wanted}')

        collection_errors = []
        for collection in self.requirements.get('collections', []):
            try:
                if collection.get('source'):
                    fqcn = collection['name']
                else:
                    fqcn = collection['fqcn']
                version_wanted = collection['version']
            except KeyError as err:
                collection_errors.append(f'Error in collection definition. Missing key: {err}')
                continue
            self._display.v(f'Checking collection "{fqcn}":')
            self._display.vv(f'Version wanted: {version_wanted}')

            try:
                collection_pkg = import_module(f'ansible_collections.{fqcn}')
            except ImportError:
                collection_errors.append(f'Collection "{fqcn}" could not be found.')
                continue

            try:
                data = load_collection_meta(collection_pkg)
            except Exception as exc:
                collection_errors.append(f'Error while loading metadata for "{fqcn}": {exc}')
                continue

            version_installed = data['version']
            self._display.vv(f'Version installed: {version_installed}')

            if version_installed != version_wanted:
                collection_errors.append(f'Collection "{fqcn}" is not installed with the correct version. '
                                         f'Installed: {version_installed} Wanted: {version_wanted}')

        if role_errors or collection_errors:
            for err in role_errors:
                self._display.error(err)
            for err in collection_errors:
                self._display.error(err)
            sys.exit(1)
