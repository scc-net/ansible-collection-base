import operator
import sys

from ansible import release
from ansible.plugins.callback import CallbackBase
from ansible.utils.version import SemanticVersion, LooseVersion

DOCUMENTATION = '''
name: check_ansible_version
callback_type: check
requirements:
    - enable in configuration
author: Dominik Rimpf
version_added: "0.4.0"
short_description: Checks the running ansible version
description:
    - Checks the ansible version against a version constraint
    - Accepts multiple conditions, eg. ">1.0,<2.0"
options:
  ansible_version:
    description: Version Constraint
    type: string
    ini:
      - section: check
        key: ansible_version
'''


def meets_requirements(version: str, requirements: str) -> bool:
    """Verify if a given version satisfies all the requirements.
    Supported version identifiers are:
      * '=='
      * '!='
      * '>'
      * '>='
      * '<'
      * '<='
      * '*'
    Each requirement is delimited by ','.
    """
    op_map = {
        '!=': operator.ne,
        '==': operator.eq,
        '=': operator.eq,
        '>=': operator.ge,
        '>': operator.gt,
        '<=': operator.le,
        '<': operator.lt,
    }

    for req in requirements.split(','):
        op_pos = 2 if len(req) > 1 and req[1] == '=' else 1
        op = op_map.get(req[:op_pos])

        requirement = req[op_pos:]
        if not op:
            requirement = req
            op = operator.eq

        if requirement == '*' or version == '*':
            continue

        if not op(
                SemanticVersion(version),
                SemanticVersion.from_loose_version(LooseVersion(requirement)),
        ):
            break
    else:
        return True

    # The loop was broken early, it does not meet all the requirements
    return False


class CallbackModule(CallbackBase):
    """
    This callback module checks if the running ansible version is allowed
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'check'
    CALLBACK_NAME = 'scc_net.base.check_ansible_version'
    CALLBACK_NEEDS_ENABLED = True

    def __init__(self, display=None, options=None):
        # make sure the expected objects are present, calling the base's __init__
        super(CallbackModule, self).__init__(display=display, options=options)

        if not self._plugin_options:
            self.set_options()

        ansible_version = release.__version__
        version_constraint = self.get_option('ansible_version')

        self._display.v('Checking ansible version...')
        if meets_requirements(ansible_version, version_constraint):
            self._display.vv(f'Ansible version "{release.__version__}" matches constraint "{version_constraint}"')
        else:
            self._display.error(
                f'Ansible version "{ansible_version}" does not comply with version constraint "{version_constraint}"')
            sys.exit(1)
