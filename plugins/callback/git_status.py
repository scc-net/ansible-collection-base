#!/usr/bin/python

import os
import datetime
from ansible.plugins.callback import CallbackBase
from ansible.errors import AnsibleError
try:
    import git
    HAVE_GIT = True
except ImportError:
    HAVE_GIT = False

__metaclass__ = type

DOCUMENTATION = '''
callback: git_status
callback_type: aggregate
requirements:
    - gitpython
short_description: Do some git sanity checks.
description:
    - This callback check git status before run plays.
options:
  behind_upstream:
    description: "Check if HEAD is behind upstream. 0: disabled, 1: warning, 2: fatal."
    type: integer
    default: 2
    choices:
      - 0
      - 1
      - 2
    ini:
      - section: git_status
        key: behind_upstream
    env:
      - name: BEHIND_UPSTREAM
  changed_files:
    description: "Check for change files. 0: disabled, 1: warning, 2: fatal."
    type: integer
    default: 1
    choices:
      - 0
      - 1
      - 2
    ini:
      - section: git_status
        key: changed_files
    env:
      - name: CHANGED_FILES
  staged_files:
    description: "Check for staged files. 0: disabled, 1: warning, 2: fatal."
    type: integer
    default: 1
    choices:
      - 0
      - 1
      - 2
    ini:
      - section: git_status
        key: staged_files
    env:
      - name: STAGED_FILES
  untracked_files:
    description: "Check for untracked files. 0: disabled, 1: warning, 2: fatal."
    type: integer
    default: 1
    choices:
      - 0
      - 1
      - 2
    ini:
      - section: git_status
        key: untracked_files
    env:
      - name: UNTRACKED_FILES
  local_commit_age_max:
    description: Maximum tolerated age of local commits in seconds. Use -1 to disable.
    type: integer
    default: 10800
    ini:
      - section: git_status
        key: local_commit_age_max
    env:
      - name: LOCAL_COMMIT_AGE_MAX
'''


class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'scc_net.base.git_status'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self, display=None, options=None):
        super(CallbackModule, self).__init__(display, options)

        if HAVE_GIT is False:
            raise AnsibleError("Can't check git status: module git is not installed")

        repo_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
        self._repo = git.Repo(repo_path)
        self._display.v("Fetching Repo...")
        self._repo.git.fetch()

        if not self._plugin_options:
            self.set_options()

        self.do_check()

    def do_check(self):
        behind_upstream = self.get_option('behind_upstream')
        if behind_upstream >= 1:
            behind_is_fatal = behind_upstream >= 2
            self.__check_commits_behind(behind_is_fatal)

        local_commit_age_max = self.get_option('local_commit_age_max')
        if local_commit_age_max >= 0:
            self.__check_commit_ahead(local_commit_age_max)

        changed_files = self.get_option('changed_files')
        if changed_files >= 1:
            changed_files_is_fatal = changed_files >= 2
            self.__check_changed_files(changed_files_is_fatal)

        staged_files = self.get_option('staged_files')
        if staged_files >= 1:
            staged_files_is_fatal = staged_files >= 2
            self.__check_staged_files(staged_files_is_fatal)

        untracked_files = self.get_option('untracked_files')
        if untracked_files >= 1:
            untracked_files_is_fatal = untracked_files >= 2
            self.__check_untracked_files(untracked_files_is_fatal)

    def __check_commits_behind(self, fatal):
        rev = "{}..{}".format(self._repo.active_branch, self._repo.remote())
        commits_behind = list(self._repo.iter_commits(rev))
        if commits_behind:
            commits_behind_messages = [
                u"{message} ({author})".format(
                    message=commit.message.split('\n')[0],
                    author=commit.author.name)
                for commit in commits_behind]
            self._display.warning("Your repo is behind {}".format(self._repo.remote()))
            self._display.display("\n".join(commits_behind_messages))
            if fatal:
                raise AnsibleError("Pull commits before continue!")

    def __check_commit_ahead(self, age_max):
        rev = "{}..{}".format(self._repo.remote(), self._repo.active_branch)
        commits_ahead = list(self._repo.iter_commits(rev))
        if commits_ahead:
            delta = max([(
                datetime.datetime.now(commit.committed_datetime.tzinfo) -
                commit.committed_datetime).total_seconds() for commit in commits_ahead])
            self._display.warning("You are {} commits ahead.".format(len(commits_ahead)))
            if delta > age_max:
                raise AnsibleError("Push commits before continue!")

    def __check_changed_files(self, fatal):
        changed_files = [item.a_path for item in self._repo.index.diff(None)]
        if changed_files:
            self._display.warning("You have changed files!")
            if fatal:
                raise AnsibleError("Add changed files before continue!")

    def __check_staged_files(self, fatal):
        staged_files = [item.a_path for item in self._repo.index.diff("HEAD")]
        if staged_files:
            self._display.warning("You have staged files!")
            if fatal:
                raise AnsibleError("Commit changes before continue!")

    def __check_untracked_files(self, fatal):
        untracked_files = self._repo.untracked_files
        if untracked_files:
            self._display.warning("You have untracked files!")
            if fatal:
                raise AnsibleError("Add unstracked files before continue!")
