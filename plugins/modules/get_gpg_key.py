#!/usr/bin/python3
__metaclass__ = type

DOCUMENTATION = r'''
---
module: get_gpg_key

short_description: Deploys gpg key to given keyring

version_added: "0.10.0"

description: This checks if the given Key ID is matching the repository, and deploys the dearmored key to the given keyring.

options:
    dearmor;
        description: Dearmor the provided key.
        required: false
        type: bool
    id:
        description: The ID or fingerprint to match the repository.
        required: true
        type: str
    url:
        description: URL to gpg key.
        required: true
        type: str
    keyring:
        description: Path to keyring.
        requred: true
        type: path

author:
    - Christoph Bluem
'''

EXAMPLES = r'''
# Add a repository
scc_net.base.better_apt_key:
    dearmor: true
    url: "https://example.com/key"
    id: "FINGERPRINT_OR_ID"
    keyring: /path/to/keyring
'''
import re
from typing import Union

from ansible.module_utils._text import to_native, to_bytes
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url


def main():
    result = dict(
        changed=False,
    )

    module = AnsibleModule(
        argument_spec=dict(
            dearmor=dict(type='bool', required=False, default=True),
            id=dict(type='str', required=True),
            url=dict(type='str', required=True),
            keyring=dict(type='path', required=True),
        ),
        supports_check_mode=True,
    )

    # local gpg binary
    global gpg_bin
    gpg_bin = module.get_bin_path('gpg', required=True)

    # parameters
    provided_key_id = sanitize_key_id(module, module.params['id'])
    url = module.params['url']
    keyring = module.params['keyring']
    dearmor = module.params['dearmor']

    # Get binary blob from keyring file
    deployed_key_data = get_keyring_data(module, keyring)

    # download key
    downloaded_key_data = download_key(module, url)

    # Check if key is armored and dearmor
    if dearmor and is_key_armored(downloaded_key_data):
        downloaded_key_data = get_binary_key_data(module, downloaded_key_data)

    # Check if deployed key is matching remote key
    # Exit if deployed key is already the correct one
    if downloaded_key_data == deployed_key_data:
        module.exit_json(msg=f"{keyring} is already present and matches binary data.", **result)

    # get key id of downloaded key
    downloaded_key_id = get_key_id(module, downloaded_key_data)
    if key_ids_matching(provided_key_id, downloaded_key_id) is not True:
        module.fail_json(
            msg=f"Provided id {provided_key_id} is not matching the id of the downloaded key {downloaded_key_id}.",
            **result)

    # We can return here already if in checkmode, no need to go further
    if deployed_key_data is None and module.check_mode:
        module.exit_json(changed=True)

    # Deploy data to keyring file
    deploy_key(module, result, downloaded_key_data, keyring)

    module.exit_json(**result)


def get_keyring_data(module, keyring) -> Union[str, None]:
    try:
        keyring_file = open(keyring, "rb")
        keyring_data = to_bytes(keyring_file.read())
    except Exception as err:
        if type(err) == FileNotFoundError:
            return None
        else:
            return module.fail_json(msg=f"There was an error opening {keyring}")
    return keyring_data


def get_key_id(module, data) -> Union[str, None]:
    if data is None or len(data) == 0:
        return None
    data_is_binary = not is_key_armored(data)
    cmd = f"{gpg_bin} --import --import-options show-only --with-colons"
    rc, out, err = module.run_command(
        cmd, data=data, binary_data=data_is_binary)
    matches = re.findall('pub.*\nfpr:+([A-F0-9]+):', out)
    if matches:
        return sanitize_key_id(module, matches[0])
    else:
        return None


def key_ids_matching(_first_key_id, _second_key_id) -> bool:
    if _first_key_id is None or _second_key_id is None:
        return False

    if _first_key_id == _second_key_id:
        return True
    return False


def download_key(module, url):
    try:
        # note: validate_certs and other args are pulled from module directly
        rsp, info = fetch_url(module, url, use_proxy=True)
        if info['status'] != 200:
            module.fail_json(
                msg=f"Failed to download key at {url}: {info['msg']}")

        return to_native(rsp.read())
    except Exception:
        module.fail_json(msg=f"error getting key id from url: {url}")


def sanitize_key_id(module, key_id) -> str:
    # make sure key is hex
    try:
        int(key_id, 16)
    except Exception:
        module.fail_json(msg=f"{key_id} is not a valid hex16 string.")

    key_id = key_id.upper()
    if key_id.startswith('0X'):
        key_id = key_id[2:]

    key_id_len = len(key_id)
    if (not (key_id_len == 8 or key_id_len == 16)) and key_id_len <= 16:
        module.fail_json(msg="'key_id must be 8, 16, or 16+ hexadecimal characters in length'")

    return key_id


def get_binary_key_data(module, data) -> bytes:
    cmd = f"{gpg_bin} --dearmor"
    rc, data, err = module.run_command(cmd, data=data)
    if rc == 1:
        module.fail_json(msg="There was an error while dearmoring the key.")

    return to_bytes(data)


def is_key_armored(data) -> bool:
    native_data = to_native(data)
    if native_data.find("-----BEGIN PGP PUBLIC KEY BLOCK-----") >= 0:
        return True
    else:
        return False


def deploy_key(module, result, data, keyring):
    try:
        with open(keyring, "wb") as keyring_file:
            keyring_file.write(to_bytes(data))
            result['changed'] = True
    except Exception:
        module.fail_json(msg=f"There was an error writing to {keyring}.")


if __name__ == '__main__':
    main()
