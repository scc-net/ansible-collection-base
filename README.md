# Ansible Collection - scc_net.base

Documentation for the collection.

## Included plugins

### scc_net.base.check_ansible_version

Checks the running ansible version against definition in `ansible.cfg`:
```
[check]
ansible_version = >=2.10.7,<2.12.0
```

### scc_net.base.check_requirements

Checks installed collections are installed with the version defined in `galaxy-requirements.yml`.

## Included roles

### scc_net.base.admin_keys

Deploys ssh keys for admins from structured data defined in vars.
Defaults to root-user.

SSH-Keys should be defined in the following form:
```yaml
ssh_keys:
  user_one:
    device_A: ssh-ed25519...
    device_B: ssh-rsa...
  user_two:
    device_C: ssh-ecdsa...
```

Admins which should get access to VMs can be defined in a list:
```yaml
admins:
  - user_one
  - user_two
```

### scc_net.base.apt_repository

[Documentation](roles/apt_repository/README.md)

### scc_net.base.locales

Configures and installs locales

### scc_net.base.netdb_client_lib

Installs current [netdb-client-lib](https://git.scc.kit.edu/scc-net/netvs/netdb-client-lib).

### scc_net.base.os_release_upgrade

[Documentation](roles/os_release_upgrade/README.md)

### scc_net.base.python_module

[Documentation](roles/python_module/README.md)

### scc_net.base.reboot

Reboots hosts

